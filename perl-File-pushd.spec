Name:          perl-File-pushd
Version:       1.016
Release:       7
Summary:       Change directory temporarily for a limited scope
License:       ASL 2.0
URL:           https://metacpan.org/release/File-pushd
Source0:       https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/File-pushd-%{version}.tar.gz
BuildArch:     noarch

BuildRequires: make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires: perl(Test::More) >= 0.96

%description
File::pushd does a temporary "chdir" that is easily and automatically
reverted, similar to "pushd" in some Unix command shells. It works by
creating an object that caches the original working directory. When the
object is destroyed, the destructor calls "chdir" to revert to the
original working directory. By storing the object in a lexical variable
with a limited scope, this happens automatically at the end of the
scope.

This is very handy when working with temporary directories for tasks
like testing; a function is provided to streamline getting a temporary
directory from File::Temp.

For convenience, the object stringifies as the canonical form of the
absolute pathname of the directory entered.

Warning: if you create multiple "pushd" objects in the same lexical
scope, their destruction order is not guaranteed and you might not wind
up in the directory you expect.


%prep
%autosetup -n File-pushd-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes CONTRIBUTING.mkdn README Todo
%{perl_vendorlib}/File/
%{_mandir}/man3/File::pushd.3*
%exclude %{_libdir}/perl5/vendor_perl/auto/File/pushd/.packlist
%exclude %{_libdir}/perl5/perllocal.pod


%changelog
* Wed Jul 17 2024 yangchenguang <yangchenguang@kylinsec.com.cn> - 1.016-7
- Modify /usr/lib64/ to %{_libdir}

* Tue Jan 14 2020 yanzhihua <yanzhihua4@huawei.com> - 1.016-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: modify spec

* Tue Jan 14 2020 daiqianwen <daiqianwen@huawei.com> - 1.016-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add build requires

* Mon Jan 06 2020 daiqianwen <daiqianwen@huawei.com> - 1.016-4
- Package init
